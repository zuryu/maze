/*
 * Further Programming Demo / Sample Solution Code
 * This code copyright (C) 2014 Dan Chalmers, University of Sussex
 */

package provided;

/**
 * A wrapper for a coordinate pair
 */
public class Place {
    protected int x;
    protected int y;
    
    @Override
    public String toString() {
	return "Place{" + "x=" + x + ", y=" + y + '}';
    }

    @Override
    public int hashCode() {
	int hash = 3;
	hash = hash * 23 + this.x;
	hash = hash * 23 + this.y;
	return hash;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final Place other = (Place) obj;
	if (this.x != other.x) {
	    return false;
	}
	if (this.y != other.y) {
	    return false;
	}
	return true;
    }
 
    public Place(int x, int y) {
	this.x = x;
	this.y = y;
    }

    public int getX() {
	return x;
    }

    public int getY() {
	return y;
    }
}
