
package mazegame7;

import java.util.Random;
import javax.swing.table.AbstractTableModel;

/**
 * This class represents a Maze.
 * 
 * It is made up of a grid of Cells and can return data on the state of each of the Cells.
 * It can reset all of the Cells in the Maze and set a random mix of blocked Cells and spaces.
 * 
 * @author Mary Gejda
 * @version 9 Feb 2014
 */
public class Maze extends AbstractTableModel {
    // The array to hold the Cells of the Maze.
    private Cell[][] maze;
    // The length of the Maze.
    private int xSize;
    // The height of the Maze.
    private int ySize;
    // The x coordinate of the start Cell.
    private int xStart;
    // The y coordinate of the start Cell.
    private int yStart;
    // The x coordinate of the end Cell.
    private int xEnd;
    // The y coordinate of the end Cell.
    private int yEnd;
    // A random variable.
    private Random rand;
    
    /**
     * Creates a new Maze of the input size and resets it so all Cells are blocked.
     * 
     * @param xSize The horizontal length of the Maze.
     * @param ySize The vertical height of the Maze.
     */
    public Maze(int xSize, int ySize){
        this.xSize = xSize;
        this.ySize = ySize;
        maze = new Cell[xSize][ySize];
        rand = new Random();
        clearMaze();
    }
    
    /**
     * Resets the Maze to a default state so all the Cells are blocked.
     */
    public void clearMaze(){
        for (int x = 0; x < xSize; x++){
            for (int y = 0; y < ySize; y++){
                maze[x][y] = new Cell(false);
            }
        }
    }
    
    /**
     * Returns true if the Cell at the specified coordinate is blocked, false otherwise.
     * 
     * @param xCoord The x (horizontal) coordinate of the Cell to be checked.
     * @param yCoord The y (vertical) coordinate of the Cell to be checked.
     * @return True if the specified Cell is blocked, false otherwise.
     */
    public boolean isBlockedAt(int xCoord, int yCoord){
        return maze[xCoord][(ySize - 1) - yCoord].isBlocked();
    }
    
    /**
     * Sets all of the cells of the Maze to be randomly blocked or spaces.
     */
    public void drawRandomMaze(){
        for (int x = 0; x < xSize; x++){
            for (int y = 0; y < ySize; y++){
                setCellAt(x, y, rand.nextBoolean());
            }
        }
    }
    
    /**
     * Returns the horizontal size of the Maze.
     * 
     * @return The horizontal size of the maze. 
     */
    @Override
    public int getColumnCount(){
        return xSize;
    }
    
    /**
     * Returns the vertical size of the Maze.
     * 
     * @return The vertical size of the maze. 
     */
    @Override
    public int getRowCount(){
        return ySize;
    }
    
    /**
     * Sets the Cell at the specified coordinates to be blocked or a space based on the input.
     * If the input coordinates are outside of the Maze this method does nothing.
     * 
     * @param xCoord The x (horizontal) coordinate of the Cell in the Maze.
     * @param yCoord The y (vertical) coordinate of the Cell in the Maze.
     * @param blocked True to set the Cell to blocked, false to set it to a space.
     */
    public void setCellAt(int xCoord, int yCoord, boolean blocked){
        if (checkForBounds(xCoord, yCoord)){
            maze[xCoord][yCoord].setBlocked(blocked);
        }
    }
    
    /**
     * Set the coordinate of the start of the Maze. If the input coordinates are outside of the Maze this method does nothing.
     * 
     * @param xStart The x (horizontal) coordinate of the start Cell.
     * @param yStart The y (vertical) coordinate of the start Cell.
     */
    public void setStart(int xStart, int yStart){
        if (checkForBounds(xStart, yStart)){
            this.xStart = xStart;
            this.yStart = (ySize - 1) - yStart;
        }
    }
    
    /**
     * Set the coordinate of the end of the Maze. If the input coordinates are outside of the Maze this method does nothing.
     * 
     * @param xEnd The x (horizontal) coordinate of the end Cell.
     * @param yEnd The y (vertical) coordinate of the end Cell.
     */
    public void setEnd(int xEnd, int yEnd){
        if (checkForBounds(xEnd, yEnd)){
            this.xEnd = xEnd;
            this.yEnd = (ySize - 1) - yEnd;
        }
    }
    
    /**
     * Return the x (horizontal) coordinate of the start Cell.
     * 
     * @return The x (horizontal) coordinate of the start Cell.
     */
    public int getXStart(){
        return xStart;
    }
    
    /**
     * Return the y (vertical) coordinate of the start Cell.
     * 
     * @return The y (vertical) coordinate of the start Cell.
     */
    public int getYStart(){
        return yStart;
    }
    
    /**
     * Return the x (horizontal) coordinate of the end Cell.
     * 
     * @return The x (horizontal) coordinate of the end Cell.
     */
    public int getXEnd(){
        return xEnd;
    }
    
    /**
     * Return the y (vertical) coordinate of the end Cell.
     * 
     * @return The y (vertical) coordinate of the end Cell.
     */
    public int getYEnd(){
        return yEnd;
    }
    
    /**
     * Returns true if the input coordinates are the same as the end Cell's coordinates.
     * 
     * @param xCheck The x (horizontal) coordinate to check against.
     * @param yCheck The y (vertical) coordinate to check against.
     * @return True if the input coordinates are the same as the end Cell's coordinates, false otherwise.
     */
    public boolean checkForEnd(int xCheck, int yCheck){
        return (xCheck == xEnd && yCheck == yEnd);
    }
    
    /**
     * Checks whether the input coordinates are within valid ranges for the Maze's array.
     * 
     * @param xCoord The x (horizontal) coordinate to be checked.
     * @param yCoord The y (vertical) coordinate to be checked.
     * @return True if the coordinates are within valid ranges for the Maze's array, false otherwise.
     */
    public boolean checkForBounds(int xCoord, int yCoord){
        return (xCoord >= 0 && xCoord < xSize && yCoord >= 0 && yCoord < ySize);
    }
    
    /**
     * Sets the cells of the maze based on the boolean values in an input array.
     * 
     * @param cells An array of boolean values that represent if the Cells of the Maze are blocked (true) or spaces (false).  
     */
    public void drawMaze(boolean[][] cells){
        if (cells.length == maze.length){
            for (int x = 0; x < xSize; x++){
                for (int y = 0; y < ySize; y++){
                    setCellAt(x, y, cells[x][y]);
                }
            }
        }
    }
    
    /**
     * Prints the Cells of the Maze in the format "[x y] blocked". Used for testing.
     */
    public void printMaze(){
        for (int x = 0; x < xSize; x++){
            System.out.println();
            for (int y = 0; y < ySize; y++){
                System.out.print("[" + x + " " + y + "] " + isBlockedAt(x, y));
            }
        }
    }

    /**
     * Returns the Cell object at the given coordinates.
     * 
     * @param rowIndex The vertical coordinate of the Cell.
     * @param columnIndex The horizontal coordinate of the Cell.
     * @return The Cell object at the given coordinates.
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return maze[columnIndex][(ySize - 1) - rowIndex];
    }
    
    /**
     * Sets a random start location in the maze making sure it is not a blocked cell.
     */
    public void setRandomStart(){
        int startX = rand.nextInt(xSize);
        int startY = rand.nextInt(ySize);
        if (checkForBounds(startX, startY) && !(isBlockedAt(startX, startY))){
            xStart = startX;
            yStart = startY;
        } else {
            setRandomStart();
        }
    }
    
    /**
     * Sets a random end location in the maze making sure it is not a blocked cell.
     */
    public void setRandomEnd(){
        int endX = rand.nextInt(xSize);
        int endY = rand.nextInt(ySize);
        if (checkForBounds(endX, endY) && !(isBlockedAt(endX, endY))){
            xEnd = endX;
            yEnd = endY;
        } else {
            setRandomEnd();
        }
    }
}
