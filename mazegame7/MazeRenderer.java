
package mazegame7;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * This class is used as a renderer for the cells in a JTable that represents a Maze. 
 * 
 * @author Mary Gejda
 * @version 4 March 2014
 */
public class MazeRenderer extends DefaultTableCellRenderer {
    // The maze to be rendered.
    private Maze maze;
    // The explorer to be rendered.
    private Explorer explorer;
    // The route that has been taken through the maze.
    private Route route;
    
    /**
     * Creates the MazeRenderer and sets the Maze and Explorer it will render.
     * 
     * @param maze The Maze to be rendered.
     * @param explorer The Explorer to be rendered.
     */
    public MazeRenderer(Maze maze, Explorer explorer, Route route){
        super();
        this.maze = maze;
        this.explorer = explorer;
        this.route = route;
    }
    
    /**
     * Renders the cells of the Maze (in a table) based on the current status of those cells.
     * Blocked cells are rendered black, spaces are rendered blue, the explorer is red, the start is 
     * light blue and the finish is light grey.
     * 
     * @param table The JTable to be rendered.
     * @param value The value of the cell to be rendered.
     * @param isSelected True if the cell is selected.
     * @param hasFocus True if the cell has focus.
     * @param row The index of the row that the cell is in.
     * @param column The index of the column that the cell is in.
     * @return The renderer for the cell.
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component cellRenderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (maze.isBlockedAt(column, row)){
            cellRenderer.setBackground(Color.BLACK);
        } else {
            cellRenderer.setBackground(Color.WHITE);
        }
        if (route.containsPlace(column, (maze.getRowCount() - 1) - row)){
            cellRenderer.setBackground(Color.CYAN);
        }
        if (route.simpleContainsPlace(column, (maze.getRowCount() - 1) - row)){
            cellRenderer.setBackground(Color.GREEN);
        }
        if (maze.getXStart() == column && maze.getYStart() == row){
            cellRenderer.setBackground(Color.BLUE);
            cellRenderer.setForeground(Color.CYAN);
            setText("Start");
        }
        if (maze.getXEnd() == column && maze.getYEnd() == row){
            cellRenderer.setBackground(Color.LIGHT_GRAY);
            cellRenderer.setForeground(Color.BLACK);
            setText("End");
        }
        if (explorer.getXPos() == column && explorer.getYPos() == row){
            cellRenderer.setBackground(Color.RED);
            cellRenderer.setForeground(Color.BLACK);
            setText("Explorer");
        }
        return this;
    }
}
