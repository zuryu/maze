
package mazegame7;

/**
 * This class represents a Cell in a Maze, a grid of Cells.
 * 
 * It can be either blocked or a space and can return its state as such.
 * It can also change its state from blocked to space or vice versa.
 * 
 * @author Mary Gejda
 * @version 9 Feb 2014
 */
public class Cell {
    //If the Cell is blocked or space.
    private boolean blocked;
    
    /**
     * Creates the Cell, making it blocked or a space based on the input boolean.
     * 
     * @param blocked If true the Cell is blocked. If false it is a space. 
     */
    public Cell(boolean blocked){
        this.blocked = blocked;
    }
    
    /**
     * Sets the Cell to blocked or a space.
     * 
     * @param blocked If true the Cell is blocked. If false it is a space.
     */
    public void setBlocked(boolean blocked){
        this.blocked = blocked;
    }
    
    /**
     * Returns whether a Cell is blocked or a space.
     * 
     * @return True if the Cell is blocked, false if it is a space. 
     */
    public boolean isBlocked(){
        return blocked;
    }
    
    @Override
    public String toString(){
        return "";
    }
}
