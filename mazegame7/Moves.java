
package mazegame7;

/**
 * The possible moves in a Maze.
 * 
 * @author Mary Gejda
 * @version 9 Feb 2014
 */
public enum Moves {
    UP, DOWN, LEFT, RIGHT;
}
