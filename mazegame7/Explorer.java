
package mazegame7;

import provided.Place;

/**
 * This class represents an Explorer in a Maze.
 * 
 * It can move north, south, east and west as long as there is an empty Cell in that direction.
 * It can be set to the start of the Maze and can return its current location in the Maze.
 * 
 * @author Mary Gejda
 * @version 9 Feb 2014
 */
public class Explorer {
    // The x (horizontal) coordinate of the Explorer.
    private int xPos;
    // The y (vertical) coordinate of the Explorer.
    private int yPos;
    //The Maze that the Explorer is exploring.
    private Maze maze;
    private Route route;
    
    /**
     * Create the Explorer at the specified coordinates in the given Maze.
     * 
     * @param xPos The x (horizontal) coordinate of the Explorer.
     * @param yPos The y (vertical) coordinate of the Explorer.
     * @param maze The Maze that the Explorer will be exploring.  
     * @param route The route that the explorer has followed.
     */
    public Explorer(int xPos, int yPos, Maze maze, Route route){
        this.xPos = xPos;
        this.yPos = yPos;
        this.maze = maze;
        this.route = route;
        route.addMove(new Place(xPos, (maze.getRowCount() - 1) - yPos));
    }
    
    /**
     * Moves the Explorer in the specified direction if the Cell in that direction is not blocked and exists within the Maze.
     * 
     * @param direction Moves enumerated type; The direction in which the Explorer should move.
     * @return True if the move was successful (i.e. not blocked and to a valid space in the Maze), false otherwise.
     */
    public boolean move(Moves direction){
        boolean successful =  false;
        switch (direction){
            case UP:
                if (maze.checkForBounds(xPos, yPos - 1) && !(maze.isBlockedAt(xPos, yPos - 1))){
                    yPos -= 1;
                    successful = true;
                    route.addMove(new Place(xPos, ((maze.getRowCount() - 1) - yPos)));
                }
                break;
            case DOWN:
                if (maze.checkForBounds(xPos, yPos + 1) && !(maze.isBlockedAt(xPos, yPos + 1))){
                    yPos += 1;
                    successful = true;
                    route.addMove(new Place(xPos, ((maze.getRowCount() - 1) - yPos)));
                }
                break;
            case LEFT:
                if (maze.checkForBounds(xPos - 1, yPos) && !(maze.isBlockedAt(xPos - 1, yPos))){
                    xPos -= 1;
                    successful = true;
                    route.addMove(new Place(xPos, ((maze.getRowCount() - 1) - yPos)));
                }
                break;
            case RIGHT:
                if (maze.checkForBounds(xPos + 1, yPos) && !(maze.isBlockedAt(xPos + 1, yPos))){
                    xPos += 1;
                    successful = true;
                    route.addMove(new Place(xPos, ((maze.getRowCount() - 1) - yPos)));
                }
                break;
            default:
                break;
        }
        return successful;
    }
    
    /**
     * Sets the position of the Explorer to the input coordinates. 
     * If the input coordinates are outside of the Maze this method does nothing.
     * 
     * @param xPos The x (horizontal) position to set the Explorer to. 
     * @param yPos The x (vertical) position to set the Explorer to.
     */
    public void setPosition(int xPos, int yPos){
        if (maze.checkForBounds(xPos, yPos)){
            this.xPos = xPos;
            this.yPos = yPos;
        }
    }
    
    /**
     * Checks if the Explorer is at the end of the maze.
     * 
     * @return True if the Explorer is at the end of the maze, false otherwise.
     */
    public boolean isAtEnd(){
        return maze.checkForEnd(xPos, yPos);
    }
    
    /**
     * Returns the x (horizontal) coordinate of the explorer.
     * 
     * @return The x (horizontal) coordinate of the explorer.
     */
    public int getXPos(){
        return xPos;
    }
    
    /**
     * Returns the y (vertical) coordinate of the explorer.
     * 
     * @return The y (vertical) coordinate of the explorer.
     */
    public int getYPos(){
        return yPos;
    }
    
    /**
     * Sets the Explorer's location back to the start of the Maze.
     */
    public void setToStart(){
        xPos = maze.getXStart();
        yPos = maze.getYStart();
    }
    
    /**
     * Returns the Maze that the explorer is exploring.
     * 
     * @return The Maze that the explorer is exploring.
     */
    public Maze getMaze(){
        return maze;
    }
    
    /**
     * Checks whether the given coordinates could be moved into by the explorer.
     * 
     * @param x The x (horizontal) coordinate to check.
     * @param y The y (vertical) coordinate to check.
     * @return True if the coordinate is not blocked, false otherwise.
     */
    public boolean validMove(int x, int y){
        return !maze.isBlockedAt(x,  (maze.getRowCount() - 1) - y);
    }
}
