
package mazegame7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import provided.Place;

/**
 * This class represents a random or best (shortest distance) walk through a Maze.
 * It can find the shortest route to the end of the maze or take a random path.
 * 
 * @author Mary Gejda
 * @version 09 April 2014
 */
public class RandomWalk {
    private Explorer explorer;
    private Route route;
    private Random rand;
    
    public RandomWalk(Explorer explorer, Route route){
        this.explorer = explorer;
        this.route = route;
        rand = new Random();
    }
    
    /**
     * Returns a random Moves enum value.
     * 
     * @return A random Moves enum value.
     */
    public Moves randomMove(){
        int moveValue = rand.nextInt(4);
        Moves move = null;
        switch (moveValue){
            case 0:
                move = Moves.UP;
                break;
            case 1:
                move = Moves.DOWN;
                break;
            case 2:
                move = Moves.LEFT;
                break;
            case 3:
                move = Moves.RIGHT;
                break;
            default:
                break;
        }
        return move;
    }
    
    /**
     * Finds a random route to the end of the Maze.
     */
    public void randomSolver(){
        while (!explorer.isAtEnd()){
            explorer.move(randomMove());
        }
        route.removeLoops();
    }
    
    /**
     * Returns a route to the end of the maze, from the Explorer's current location, that has the 
     * fewest steps possible. 
     * 
     * @return The route to the end of the maze with the fewest steps possible.
     */
    public Route bfSearch(){
        Queue<Route> toDo =  new LinkedList<Route>();
        ArrayList<Route> visitedRoutes = new ArrayList<Route>();
        ArrayList<Route> temp = new ArrayList<Route>();
        Route startRoute = new Route();
        boolean inRange = true;
        startRoute.addMove(new Place(explorer.getXPos(), (explorer.getMaze().getRowCount() - 1) - explorer.getYPos()));
        toDo.add(startRoute);
        while(!toDo.isEmpty()){
            startRoute = toDo.remove();
            if (startRoute.containsPlace(explorer.getMaze().getXEnd(), (explorer.getMaze().getRowCount() - 1) - explorer.getMaze().getYEnd())){
                System.out.println();
                System.out.println();
                System.out.println(startRoute);
                return startRoute;
            } else {
                visitedRoutes.add(startRoute);
                temp = startRoute.getChildren();
                for (Route child : temp){
                    inRange = child.getRouteList().get(child.getSize() - 1).getX() >= 0 
                            && child.getRouteList().get(child.getSize() - 1).getX() < explorer.getMaze().getColumnCount() 
                            && child.getRouteList().get(child.getSize() - 1).getY() >= 0 
                            && child.getRouteList().get(child.getSize() - 1).getY() < explorer.getMaze().getRowCount()
                            && explorer.validMove(child.getRouteList().get(child.getSize() - 1).getX(), child.getRouteList().get(child.getSize() - 1).getY());
                    if (!toDo.contains(child) && !visitedRoutes.contains(child) && inRange){
                        toDo.add(child);
                    }
                }
            }
        }
        return new Route();
    }
}