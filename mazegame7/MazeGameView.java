
package mazegame7;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import provided.Place;

/**
 * This class draws the user interface for a maze game.
 * 
 * @author Mary Gejda
 * @version 5 March 2014
 */
public class MazeGameView {
    //The main window.
    private JFrame frame;
    // The conyainer holding all the other components of the user interface.
    private Container contentPane;
    // The maze that is being drawn.
    private Maze maze;
    // The explorer that is being drawn.
    private Explorer explorer;
    // The JTable representing the maze. 
    private JTable mazeTable;
    // The renderer for the cells of the maze.
    private MazeRenderer cellRender;
    // A JLabel to display status messages.
    private JLabel statusLabel;
    // A random variable.
    private Random rand;
    // The route taken through the maze.
    private Route route;
    private RandomWalk solver;
    
    public static void main(String[] args){
        new MazeGameView(10, 20);
    }
    /**
     * Creates MazeGameView object, which in turn creates the user interface for the maze game.
     * 
     * @param xSize The horizontal size of the maze.
     * @param ySize The vertical size of the maze.
     */
    MazeGameView(int xSize, int ySize){
        maze = new Maze(xSize, ySize);
        maze.drawRandomMaze();
        maze.setRandomStart();
        maze.setRandomEnd();
        route = new Route();
        explorer = new Explorer(maze.getXStart(), maze.getYStart(), maze, route);
        route.addMove(new Place(explorer.getXPos(), (maze.getRowCount() - 1) - explorer.getYPos()));
        cellRender = new MazeRenderer(maze, explorer, route);
        mazeTable = new JTable(maze);
        makeGUI();
        maze.printMaze();
        rand = new Random();
        solver = new RandomWalk(explorer, route);
    }
    
    /*
     * Sets up the main user interface for the maze.
     */
    private void makeGUI(){
        frame = new JFrame("Maze Game");
        contentPane = frame.getContentPane();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        drawMazeGUI();
        drawMovementButtons();
        drawStatusLabel();
        drawMenuBar();
        
        contentPane.setPreferredSize(new Dimension(700, 700));
        frame.pack();
        frame.setVisible(true);
    }
    
    /*
     * Draws the JTable which represents the maze. 
     */
    private void drawMazeGUI(){
        JPanel mazePanel = new JPanel();
        
        mazeTable.setRowHeight(50);
        mazePanel.add(mazeTable);
        
        setRenderer();
        
        JScrollPane mazePanelScroll = new JScrollPane(mazePanel);
        contentPane.add(mazePanelScroll);        
    }
    
    /*
     * Sets up the renderer for each column of the maze. 
     */
    private void setRenderer(){
        TableColumn column;
        for (int x = 0; x < maze.getColumnCount(); x++){
            column = mazeTable.getColumnModel().getColumn(x);
            column.setPreferredWidth(50);
            column.setCellRenderer(cellRender);
        }
    }
    
    /*
     * Draws the movement buttons and sets up listeners for them. 
     */
    private void drawMovementButtons(){
        JPanel buttonContainer = new JPanel();
        buttonContainer.setLayout(new BorderLayout());
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(0, 1));
        
        JPanel upPanel = new JPanel();
        upPanel.setLayout(new FlowLayout());
        JButton upButton = new JButton("Up");
        upButton.setPreferredSize(new Dimension(70, 70));
        upButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                move(Moves.UP);
                atEndMessage();
            }
        }); 
        upPanel.add(upButton);
        buttonPanel.add(upPanel);
        
        JPanel midPanel = new JPanel();
        midPanel.setLayout(new FlowLayout());
        JButton leftButton = new JButton("Left");
        leftButton.setPreferredSize(new Dimension(70, 70));
        leftButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                move(Moves.LEFT);
                atEndMessage();
            }
        }); 
        midPanel.add(leftButton);
        
        JButton rightButton = new JButton("Right");
        rightButton.setPreferredSize(new Dimension(70, 70));
        rightButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                move(Moves.RIGHT);
                atEndMessage();
            }
        }); 
        midPanel.add(rightButton);
        buttonPanel.add(midPanel);
        
        JPanel downPanel = new JPanel();
        downPanel.setLayout(new FlowLayout());
        JButton downButton = new JButton("Down");
        downButton.setPreferredSize(new Dimension(70, 70));
        downButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                move(Moves.DOWN);
                atEndMessage();
            }
        }); 
        frame.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_W || e.getKeyCode() == KeyEvent.VK_UP){
                    move(Moves.UP);
                    atEndMessage();
                }
                if (e.getKeyCode() == KeyEvent.VK_S || e.getKeyCode() == KeyEvent.VK_DOWN){
                    move(Moves.DOWN);
                    atEndMessage();
                }
                if (e.getKeyCode() == KeyEvent.VK_A || e.getKeyCode() == KeyEvent.VK_LEFT){
                    move(Moves.LEFT);
                    atEndMessage();
                }
                if (e.getKeyCode() == KeyEvent.VK_D || e.getKeyCode() == KeyEvent.VK_RIGHT){
                    move(Moves.RIGHT);
                    atEndMessage();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        frame.setFocusable(true);
        downPanel.add(downButton);
        buttonPanel.add(downPanel);
        buttonContainer.add(buttonPanel, BorderLayout.NORTH);
        
        contentPane.add(buttonPanel, BorderLayout.EAST);
    }
    
    /*
     * Sets up the status label with a default message.
     */
    private void drawStatusLabel(){
        statusLabel = new JLabel("The red square is your explorer. The grey square is the end of the maze.");
        statusLabel.setBorder(new EmptyBorder(10,10,10,10));
        
        contentPane.add(statusLabel, BorderLayout.SOUTH);
    }
    
    /*
     * Moves the explorer and sets the status label text to a message if the move fails.
    
     * @param move The enumerated type of the direction to move.
     */
    private void move(Moves move){
        if (!(explorer.move(move))){
            statusLabel.setText("You can't move that way.");
        } else {
            statusLabel.setText(" ");
        }
        mazeTable.repaint();
    }
    
    /*
     * Sets up the menu bar. 
     */
    private void drawMenuBar(){
        JMenuBar menuBar = new JMenuBar();
        
        JMenu gameMenu = new JMenu("Game");
        menuBar.add(gameMenu);
        
        JMenuItem newMaze8Item = new JMenuItem("8x8 Grid");
        newMaze8Item.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                default8Grid();
            }
        });
        gameMenu.add(newMaze8Item);
        
        JMenuItem newMaze10Item = new JMenuItem("10x20 Grid");
        newMaze10Item.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                default10Grid();
            }
        });
        gameMenu.add(newMaze10Item);
        
        JMenuItem newMazeFaceItem = new JMenuItem("Face Grid");
        newMazeFaceItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                faceGrid();
            }
        });
        gameMenu.add(newMazeFaceItem);
        
        JMenuItem newMazeRandItem = new JMenuItem("Random Grid");
        newMazeRandItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = rand.nextInt(41);
                int y = rand.nextInt(41);
                setUpNewRandomMaze(x, y);
            }
        });
        gameMenu.add(newMazeRandItem);
        
        JMenuItem resetItem = new JMenuItem("Reset");
        resetItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                explorer.setToStart();
                route.clearRoute();
                mazeTable.repaint();
            }
        });
        gameMenu.add(resetItem);
        
        JMenu solverMenu = new JMenu("Solver");
        menuBar.add(solverMenu);
        
        JMenuItem basicSolverItem = new JMenuItem("Basic solver");
        basicSolverItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                solver.randomSolver();
                mazeTable.repaint();
            }
        });
        solverMenu.add(basicSolverItem);
        
        JMenuItem bestSolverItem = new JMenuItem("Best solver");
        bestSolverItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                route.getSimpleRoute().clear();
                route.getSimpleRoute().addAll(solver.bfSearch().getRouteList());
                mazeTable.repaint();
            }
        });
        solverMenu.add(bestSolverItem);
        
        JMenu helpMenu = new JMenu("Help");
        menuBar.add(helpMenu);
        
        JMenuItem controlItem = new JMenuItem("Controls");
        controlItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "W - move up \n D - move right \n S - move down \n A - move left", "Controls", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        helpMenu.add(controlItem);
        
        JMenuItem aboutItem = new JMenuItem("About");
        aboutItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "Maze Game V0.8 \n by 118435", "About", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        helpMenu.add(aboutItem);
        
        frame.setJMenuBar(menuBar);
    }
    
    /*
     * Creates a new maze of the given dimensions.
     * 
     * @param xSize The horizontal size of the maze.
     * @param ySize The vertical size of the maze.
     */
    private void setUpNewRandomMaze(int xSize, int ySize){
        maze = new Maze(xSize, ySize);
        maze.drawRandomMaze();
        maze.setRandomStart();
        maze.setRandomEnd();
        route = new Route();
        explorer = new Explorer(maze.getXStart(), maze.getYStart(), maze, route);
        mazeTable.setModel(maze);
        route.addMove(new Place(explorer.getXPos(), (maze.getRowCount() - 1) - explorer.getYPos()));
        cellRender = new MazeRenderer(maze, explorer, route);
        setRenderer();
        statusLabel.setText("The red square is your explorer. The grey square is the end of the maze.");
        solver = new RandomWalk(explorer, route);
    }
    
    /*
     * Sets the message on the status label when the end of the maze is reached.
     */
    private void atEndMessage(){
        if (explorer.isAtEnd()){
            statusLabel.setText("Congratulations, you have found the end of the maze!");
        }
    }
    
     /*
     * Draws a default 10x20 maze. 
     */
    private void default10Grid(){
        maze = new Maze(10, 20);
        boolean[][] defaultMazeB = new boolean[][] {{true,  false,  true,   true,  true,   true,  true,   true,   false, true,  true,   true,   true,   true,   true,   true,   true,   true,   true,   true},
                                                    {true,  false,  false,  false, false,  true,  false,  false,  false, true,  false,  false,  false,  true,   true,   true,   true,   true,   false,  false},
                                                    {true,  true,   true,   true,  false,  true,  true,   false,  true,  true,  true,   true,   false,  true,   true,   true,   true,   true,   false,  true}, 
                                                    {true,  false,  false,  false, false,  true,  true,   false,  true,  false, false,  false,  false,  false,  false,  false,  false,  false,  false,  true},
                                                    {true,  true,   false,  true,  false,  true,  true,   false,  true,  false, true,   false,  true,   true,   true,   false,  true,   true,   true,   true},
                                                    {true,  true,   false,  true,  false,  false, true,   false,  true,  true,  true,   true,   true,   false,  true,   false,  true,   false,  true,   true},
                                                    {true,  true,   false,  true,  true,   false, false,  false,  false, false, false,  true,   true,   false,  false,  false,  false,  false,  false,  true},
                                                    {true,  false,  false,  true,  true,   true,  false,  true,   true,  true,  true,   true,   true,   true,   true,   false,  true,   true,   true,   true},
                                                    {false, false,  true,   true,  true,   true,  false,  false,  false, false, false,  false,  false,  false,  false,  false,  false,  true,   true,   true},
                                                    {false, true,   true,   true,  true,   true,  false,  true,   true,  true,  true,   true,   true,   true,   true,   true,   true,   true,   true,   true}};
        maze.drawMaze(defaultMazeB);
        maze.setRandomStart();
        maze.setRandomEnd();
        route = new Route();
        explorer = new Explorer(maze.getXStart(), maze.getYStart(), maze, route);
        mazeTable.setModel(maze);
        route.addMove(new Place(explorer.getXPos(), (maze.getRowCount() - 1) - explorer.getYPos()));
        cellRender = new MazeRenderer(maze, explorer, route);
        setRenderer();
        statusLabel.setText("The red square is your explorer. The grey square is the end of the maze."); 
        solver = new RandomWalk(explorer, route);
    }
    
    /*
     * Draws a default 8x8 maze. 
     */
    private void default8Grid(){
        maze = new Maze(8, 8);
        boolean[][] defaultMazeA = new boolean[][] {{true,  false,  true,   true,   true,   true,   true,   false},
                                                    {true,  false,  true,   false,  false,  false,  true,   false},
                                                    {true,  false,  true,   false,  true,   false,  true,   false},
                                                    {true,  false,  false,  false,  true,   false,  true,   false},
                                                    {true,  true,   true,   true,   true,   false,  true,   false},
                                                    {true,  false,  false,  false,  false,  false,  false,  false},
                                                    {false, false,  true,   false,  true,   true,   false,  true},
                                                    {true,  true,   true,   false,  true,   true,   false,  true}};
        maze.drawMaze(defaultMazeA);
        maze.setRandomStart();
        maze.setRandomEnd();
        route = new Route();
        explorer = new Explorer(maze.getXStart(), maze.getYStart(), maze, route);
        mazeTable.setModel(maze);
        route.addMove(new Place(explorer.getXPos(), (maze.getRowCount() - 1) - explorer.getYPos()));
        cellRender = new MazeRenderer(maze, explorer, route);
        setRenderer();
        statusLabel.setText("The red square is your explorer. The grey square is the end of the maze.");
        solver = new RandomWalk(explorer, route);
    }
    
    /*
     * A face Maze.
     */
    private void faceGrid(){
        maze = new Maze(8, 8);
        maze.setCellAt(8, 8, true);
	maze.setCellAt(1, 7, true);
	maze.setCellAt(2, 7, true);
	maze.setCellAt(5, 7, true);
	maze.setCellAt(6, 7, true);
	maze.setCellAt(1, 5, true);
	maze.setCellAt(2, 5, true);
	maze.setCellAt(1, 4, true);
	maze.setCellAt(2, 4, true);
	maze.setCellAt(5, 5, true);
	maze.setCellAt(6, 5, true);
	maze.setCellAt(5, 4, true);
	maze.setCellAt(6, 4, true);
	maze.setCellAt(4, 3, true);
	maze.setCellAt(4, 2, true);
	maze.setCellAt(1, 2, true);
	maze.setCellAt(1, 1, true);
	maze.setCellAt(6, 2, true);
	maze.setCellAt(6, 1, true);
	maze.setCellAt(1, 1, true);
	maze.setCellAt(2, 0, true);
	maze.setCellAt(3, 0, true);
	maze.setCellAt(4, 0, true);
	maze.setCellAt(5, 0, true);
	maze.setStart(0, 0);
	maze.setEnd(3, 2);
        route = new Route();
        explorer = new Explorer(maze.getXStart(), maze.getYStart(), maze, route);
        mazeTable.setModel(maze);
        route.addMove(new Place(explorer.getXPos(), (maze.getRowCount() - 1) - explorer.getYPos()));
        cellRender = new MazeRenderer(maze, explorer, route);
        setRenderer();
        statusLabel.setText("The red square is your explorer. The grey square is the end of the maze.");
        solver = new RandomWalk(explorer, route);
    }
}

