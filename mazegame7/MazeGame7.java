
package mazegame7;

/**
 * This class contains a main method to run a maze game. The main method has been moved to MazeGameView class.
 * 
 * @author Mary Gejda
 * @version 9 Feb 2014
 */
public class MazeGame7 {

    /**
     * @param args the command line arguments
     */
    /* public static void main(String[] args) {
    //testMaze1();
    }*/
    
    public static void testMaze1(){
        Maze mazeA = new Maze(8, 8);
        Maze mazeB = new Maze(10, 20);
        boolean[][] defaultMazeA = new boolean[][] {{true,  false,  true,   true,   true,   true,   true,   false},
                                                    {true,  false,  true,   false,  false,  false,  true,   false},
                                                    {true,  false,  true,   false,  true,   false,  true,   false},
                                                    {true,  false,  false,  false,  true,   false,  true,   false},
                                                    {true,  true,   true,   true,   true,   false,  true,   false},
                                                    {true,  false,  false,  false,  false,  false,  false,  false},
                                                    {false, false,  true,   false,  true,   true,   false,  true},
                                                    {true,  true,   true,   false,  true,   true,   false,  true}};
        boolean[][] defaultMazeB = new boolean[][] {{true,  false,  true,   true,  true,   true,  true,   true,   false, true,  true,   true,   true,   true,   true,   true,   true,   true,   true,   true},
                                                    {true,  false,  false,  false, false,  true,  false,  false,  false, true,  false,  false,  false,  true,   true,   true,   true,   true,   false,  false},
                                                    {true,  true,   true,   true,  false,  true,  true,   false,  true,  true,  true,   true,   false,  true,   true,   true,   true,   true,   false,  true}, 
                                                    {true,  false,  false,  false, false,  true,  true,   false,  true,  false, false,  false,  false,  false,  false,  false,  false,  false,  false,  true},
                                                    {true,  true,   false,  true,  false,  true,  true,   false,  true,  false, true,   false,  true,   true,   true,   false,  true,   true,   true,   true},
                                                    {true,  true,   false,  true,  false,  false, true,   false,  true,  true,  true,   true,   true,   false,  true,   false,  true,   false,  true,   true},
                                                    {true,  true,   false,  true,  true,   false, false,  false,  false, false, false,  true,   true,   false,  false,  false,  false,  false,  false,  true},
                                                    {true,  false,  false,  true,  true,   true,  false,  true,   true,  true,  true,   true,   true,   true,   true,   false,  true,   true,   true,   true},
                                                    {false, false,  true,   true,  true,   true,  false,  false,  false, false, false,  false,  false,  false,  false,  false,  false,  true,   true,   true},
                                                    {false, true,   true,   true,  true,   true,  false,  true,   true,  true,  true,   true,   true,   true,   true,   true,   true,   true,   true,   true}};
                                                    
        mazeA.drawMaze(defaultMazeA);
        mazeB.drawMaze(defaultMazeB);
        mazeB.printMaze();
        System.out.println();
        mazeA.printMaze();
    }
}
