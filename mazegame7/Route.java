
package mazegame7;

import java.util.ArrayList;
import java.util.Objects;
import provided.Place;

/**
 * This class represents a route through a maze. It holds the coordinates of the route
 * and can add new coordinates. It can clear the current route and remove any loops 
 * within the route.
 * 
 * @author Mary Gejda
 * @version 5 March 2014
 */
public class Route {
    // The list of all of the coordinates of the cell in the maze that have been visited so far in the format "x,y".
    private ArrayList<Place> route;
    // This is the route displayed in green when the solver (RandomWalk class) is used to solve the maze.
    private ArrayList<Place> simpleRoute;
    
    /**
     * Creates a new empty route.
     */
    public Route(){
        route = new ArrayList<Place>();
        simpleRoute = new ArrayList<Place>();
    }
    
    /**
     * Adds a set of coordinates to the route.
     * 
     * @param move The coordinates to be added; a String in the form "x,y". 
     */
    public void addMove(Place move){
        route.add(move);
    }
    
    /**
     * Clears or empties the route of coordinates.
     */
    public void clearRoute(){
        route.clear();
        simpleRoute.clear();
    }
    
    /**
     * Removes any coordinate on the route which form part of a loop i.e. all coordinates between 
     * two identical coordinates. One of the duplicate coordinates is left in the route.
     */
    public void removeLoops(){
        simpleRoute.clear();
        simpleRoute.addAll(route);
        int nextStep;
        for (int i = 0; i < simpleRoute.size(); i++){
            nextStep = getNextDuplicateStep(i);
            if (nextStep != -1){
                simpleRoute.subList(i, nextStep).clear();
                i--;
            }
        }
    }
    
    /**
     * Checks if there is any other occurrence a particular coordinate within the route and returns the 
     * index value of that coordinate. If no duplicate coordinate is found it returns -1.
     * 
     * @param stepIndex The index of the coordinate to check for.
     * @return The index of the first duplicate of the given coordinate or -1 if no duplicate exists.
     */
    public int getNextDuplicateStep(int stepIndex){
        boolean found = false;
        int i = (stepIndex + 1);
        while(!found && i < simpleRoute.size()){
            found = (simpleRoute.get(stepIndex).equals(simpleRoute.get(i)));
            i++;
        }
        if (found){
            return --i;
        } else {
            return -1;
        }
    }
    
    /**
     * Prints the list of coordinates on the route. Used for testing.
     */
    public void print(){
        for (Place place : route){
            System.out.println(place);
        }
    }
    
    /**
     * Returns the number of steps (coordinates) in the route.
     * 
     * @return The number of coordinates in the route. 
     */
    public int getSize(){
        return route.size();
    }
    
    /**
     * Returns the number of steps (coordinates) in the simple route.
     * 
     * @return The number of coordinates in the simple route. 
     */
    public int getSimpleRouteSize(){
        return simpleRoute.size();
    }
    
    /**
     * Returns the coordinates at a given step in the route. If the step doesn't exist then null is returned.
     * 
     * @param stepIndex The number of steps before the coordinates to return. 
     * @return The coordinate at the given step. Null if the step is not in the Route.
     */
    public Place getStepAt(int stepIndex){
        if (checkIndexRange(stepIndex)){
            return route.get(stepIndex);
        } else {
            return null;
        }
    }
    
    /**
     * Returns the coordinates at a given step in the route. If the step doesn't exist then null is returned.
     * 
     * @param stepIndex The number of steps before the coordinates to return. 
     * @return The coordinate at the given step. Null if the step is not in the Route.
     */
    public Place getSimpleRouteStepAt(int stepIndex){
        if (checkIndexRange(stepIndex)){
            return simpleRoute.get(stepIndex);
        } else {
            return null;
        }
    }
    
    /**
     * Returns true if a given index value is valid within the route, false otherwise.
     * 
     * @param index The index value to be checked.
     * @return True if index is within a valid range for the route, false otherwise.
     */
    public boolean checkIndexRange(int index){
        return (index >= 0 && index < route.size());
    }
    
    /**
     * Returns true if the route contains the given coordinates.
     * 
     * @param x The x coordinate of the Cell to be checked.
     * @param y The y coordinate of the Cell to be checked.
     * @return True if the route contains a Place with the coordinates x and y, false otherwise.
     */
    public boolean containsPlace(int x, int y){
        for (Place place : route){
            if (place.toString().equals("Place{x=" + x + ", y=" + y + "}")){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Returns true if the simple route contains the given coordinates.
     * 
     * @param x The x coordinate of the Cell to be checked.
     * @param y The y coordinate of the Cell to be checked.
     * @return True if the simple route contains a Place with the coordinates x and y, false otherwise.
     */
    public boolean simpleContainsPlace(int x, int y){
        for (Place place : simpleRoute){
            if (place.toString().equals("Place{x=" + x + ", y=" + y + "}")){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Returns a list of all the coordinates in the route.
     * 
     * @return A list of all the coordinates in the route.
     */
    public ArrayList<Place> getRouteList(){
        return route;
    }
    
    /**
     * Returns a list of all the coordinates in the simple route.
     * 
     * @return A list of all the coordinates in the simple route.
     */
    public ArrayList<Place> getSimpleRoute(){
        return simpleRoute;
    }
    
    /**
     * Sets the simple route to the given arrayList.
     * 
     * @param simpleRoute An ArrayList of Place which will be the new simple route.
     */
    public void setSimpleRoute(ArrayList<Place> simpleRoute){
        this.simpleRoute = simpleRoute;
    }
    
    /**
     * Returns a list of routes that could be created by moving from the last Place in the current Route.
     * 
     * @return A list of routes that could be created by moving from the last Place in the current Route.
     */
    public ArrayList<Route> getChildren(){
        ArrayList<Route> children = new ArrayList<Route>();
        for (int i = 0; i < 4; i++){
            Route child = new Route();
            child.route.addAll(this.route);
            switch (i){
                case 0:     //UP
                    child.addMove(new Place(child.route.get(child.route.size() - 1).getX(), (child.route.get(child.route.size() - 1).getY()) - 1));
                    children.add(child);
                    break;
                case 1:     //DOWN
                    child.addMove(new Place(child.route.get(child.route.size() - 1).getX(), (child.route.get(child.route.size() - 1).getY()) + 1));
                    children.add(child);
                    break;
                case 2:     //LEFT
                    child.addMove(new Place((child.route.get(child.route.size() - 1).getX() - 1), child.route.get(child.route.size() - 1).getY()));
                    children.add(child);
                    break;
                case 3:     //RIGHT
                    child.addMove(new Place((child.route.get(child.route.size() - 1).getX() + 1), child.route.get(child.route.size() - 1).getY()));
                    children.add(child);
                    break;
                default:
                    break;
            }
        }
        return children;
    }

    /**
     * Returns a string representation of the route.
     * 
     * @return A string representation of the route. 
     */
    @Override
    public String toString() {
        return route + "";
    }  

    /**
     * Returns the hash code of the Route.
     * @return The hash code of the Route.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.route);
        return hash;
    }

    /**
     * Checks whether the Route is equal to another object.
     * 
     * @param obj The object to compare the Route to.
     * @return True if the Route has the same route as the object, false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Route other = (Route) obj;
        if (!Objects.equals(this.route, other.route)) {
            return false;
        }
        return true;
    }
}
